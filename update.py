import os
import argparse
import json
import subprocess
from distutils.version import LooseVersion


class GitUpdate:
    dirlist = []
    entryPath = '/var/www/demos/gitupdate/'
    package = ""
    version_from = ""
    version_to = ""

    def __init__(self):
        parser = argparse.ArgumentParser(description='Provide package and which version you want to update!')
        parser.add_argument('path', metavar='path', help='Path where all git repos are being pulled')
        parser.add_argument('package', metavar='package', help='Package name')
        parser.add_argument('version_from', metavar='version_from', help='Version to update to')
        parser.add_argument('version_to', metavar='version_to', help='Version to update from')
        args = parser.parse_args()

        self.entryPath = args.path
        self.package = args.package
        self.version_from = args.version_from
        self.version_to = args.version_to
        self.dirlist = self.listdirs(self.entryPath)

        self.displayDescription()

    def listdirs(self, folder):
        return [
            d for d in (os.path.join(folder, d1) for d1 in os.listdir(folder))
            if os.path.isdir(d)
        ]

    def displayDescription(self):
        self.output_line('=')
        print('Trying to update')
        print('Path      ' + self.entryPath)
        print('Package   ' + self.package)
        print('Version   ' + self.version_from + " => " + self.version_to)
        # self.output_line('-')
        # print('Directories')
        # for (dir) in (self.dirlist):
        # print(' - ' + dir)
        # self.output_line('-')
        self.output_line('=')

    def run(self):
        for (dir) in (self.dirlist):
            self.output_line('-', 30)
            print('PROJECT ' + os.path.basename(dir))

            if os.path.isfile(dir + '/composer.json'):
                self.updateSingleProject(dir)

            else:
                print ("   ERROR: no composer.json found!")

    def updateSingleProject(self, directory):
        packageFound = False
        startUpdate = False

        composerFile = directory + '/composer.lock'
        jsonx = json.loads(open(composerFile).read())
        # print jsonx['packages']
        for (p) in (jsonx['packages']):
            if p['name'] == self.package:

                if LooseVersion(p['version']) == LooseVersion(self.version_to):
                    packageFound = True
                    print('  Version ' + self.version_to + ' already in use!')
                else:
                    packageFound = True
                    startUpdate = True
                    print ("   Project uses version " + p['version'])

        if not packageFound:
            print("SORRY, package not found!!")
        if startUpdate:
            self.git_pull(directory)
            self.startUpdateProcess(directory)

    def git_pull(self, directory):
        command = 'git -C ' + directory + ' reset --hard'
        p = self.command(command)

        command = 'git -C ' + directory + ' pull'
        p = self.command(command)
        # print(p.stdout.read())

    def git_push(self, directory):
        command = 'git -C ' + directory + ' add composer.json composer.lock'
        p = self.command(command)
        print(p.stdout.read())

        command = 'git -C ' + directory + ' commit -m "[TASK] Update ' + self.package + ' to ' + self.version_to + '"'
        p = self.command(command)
        print(p.stdout.read())

        command = 'git -C ' + directory + ' push'
        p = self.command(command)
        print(p.stdout.read())

    def startUpdateProcess(self, directory):
        command = '/usr/local/bin/composer.phar require "' + self.package + ':' + self.version_to + '" --working-dir=' + directory

        p = self.command(command)
        text = p.stdout.read()
        out = ''

        if text.find('Nothing to install or update') != -1:
            out = '   No change required!'
        elif text.find('Your requirements could not be resolved to an installable set of packages') != -1:
            out = '   A problem occurred'
        elif text.find('Updating dependencies') != -1:
            out = '   Update worked!'
            self.git_push(directory)

        print (out)

    def command(self, command):
        return subprocess.Popen(command, universal_newlines=True, shell=True,
                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    def output_line(self, character='-', max=50):
        line = ''
        for x in range(0, max):
            line += character

        print(line)


x = GitUpdate()
x.run()
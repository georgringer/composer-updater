# Multi composer update

Use this python script to update packages in multiple projects including a push using GIT.

## How to use

Use the command line like: ::

    python3 ./update.py /var/www/demos/gitupdate typo3/cms '6.2.*' 6.2.3

Arguments are:

* Path to the directory where all repos are
* Package
* Version which should be updated
* Desired version

## License

This script is licensed under MIT